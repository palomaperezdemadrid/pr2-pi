/**
Copyright [año] [nombre del propietario del copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
**/

package montecarlo;

public class MonteCarlo {

	public double pi; 

	private boolean elPuntoHaEntrado(){

		double x = 0;
 		double y = 0;

			x = Math.random()*(-2)+1;
			y = Math.random()*(-2)+1;

			return (x*x + y*y)<1; 
		}

	private double hallarPuntosCirculo(double puntoTotales){
		   double puntosCirculo = 0;
		   double resto = puntoTotales;

			if(resto==0){
				return 0;

			}else{

			 if(elPuntoHaEntrado() == true){
				return puntosCirculo = puntosCirculo + 1;
			}
			resto = resto -1;

			if (resto >= 1) {
				puntosCirculo = puntosCirculo + hallarPuntosCirculo(resto);
			}
		}
   	return puntosCirculo;
   }

	public double calcularNumeroPi(int puntosLanzados){
	//return pi = 4.00 * ((double)puntosCirculo/puntosLanzados);
		return pi = 4.00 *((double)hallarPuntosCirculo(puntosLanzados)/puntosLanzados);
		}

	}


//PLATEAMIENTO

//si n==(x * x) + (y * y)<1 --> suma 1 + metodoMontecarlo (n-1)
//para calcular pi--> pi = 4.00*((double)puntosCirculo/puntosLanzados);   
