# README #

Este programa se encarga de calcular el número pi

## ¿En qué se basa? ##
Según el numero de lanzamientos, si calculamos el numero pi

## ¿Cómo uso el programa? ##

 `java -jar catalogo.jar [numero de lanzamientos] `


## ¿Cómo instalo el sistema? ##

- 1 Realice un git clone desde el repositorio
- 2 Abra en su terminal  pr2-pi y entre 

`cd pr2-pi ` 

- 3 escriba en su terminal 

`make jar `

- 4 ya puede escribir 

`java -jar montecarlo.jar __________`

## Realizar el javadoc ##
`make javadoc `

## Diagrama de clases ##

![](catalogorepo/diagrama_de_clases.png)

### Creador ###
Paloma Pérez de Madrid 
> 1º de Grado de Ingeniería de Sistemas de Informacion, Universidad San Pablo CEU
